using System.Collections;
using System.Collections.Generic;
//using AmazingAssets.AdvancedDissolve;
using UnityEngine;

public class AnimateDissolve : MonoBehaviour
{
    // 마테리얼 인스턴스
    Material[] material;

    float offset;
    float speed;
    float clip;

    public bool onHit;
    
    private void OnEnable()
    {
        GetComponent<Renderer>();
        
        material[0] = GetComponent<Renderer>().material;
        material[1] = GetComponentInChildren<Renderer>().material;
        
        offset = Random.value;
        speed = Random.Range(0.1f, 0.2f);
        
        foreach (var t in material)
        {
            //AdvancedDissolveProperties.Cutout.Standard.UpdateLocalProperty(t, AdvancedDissolveProperties.Cutout.Standard.Property.Clip, 0);
        }
        
        onHit = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!onHit) return;
        
        //float clip = Mathf.PingPong(offset + Time.time * speed, 1);
        if (clip >= offset)
        {
            onHit = false;

            //ObjectPool.instance.cubeRed.Enqueue(transform.gameObject);
        }
    }

    public void CutOutStart()
    {
        onHit = false;
        
        clip = Mathf.Lerp(0, offset, speed * Time.deltaTime);
        
        foreach (var t in material)
        {
            //AdvancedDissolveProperties.Cutout.Standard.UpdateLocalProperty(t, AdvancedDissolveProperties.Cutout.Standard.Property.Clip, clip);
        }
    }

    public IEnumerator CutOutcor()
    {
        onHit = true;
        
        while (onHit)
        {
            clip = Mathf.Lerp(0.1f, offset, speed * Time.deltaTime);
            
            foreach (var t in material)
            {
                //AdvancedDissolveProperties.Cutout.Standard.UpdateLocalProperty(t, AdvancedDissolveProperties.Cutout.Standard.Property.Clip, clip);
            }
        }
        
        foreach (var t in material)
        {
            //AdvancedDissolveProperties.Cutout.Standard.UpdateLocalProperty(t, AdvancedDissolveProperties.Cutout.Standard.Property.Clip, 1);
        }
        
        yield return null;
        transform.gameObject.SetActive(false);
    }



}
