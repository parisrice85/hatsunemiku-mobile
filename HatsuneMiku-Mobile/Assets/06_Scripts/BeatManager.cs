using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BeatManager : MonoBehaviour
{
    public float _bpm;                    // 곡의 BPM
    [SerializeField] private AudioSource _audioSource;      // 기준이 되는 오디오
    [SerializeField] private Intervals[] _intervals;        // 

    private void Update()
    {
        foreach (Intervals interval in _intervals)
        {
            // timeSamples => 음악이 느려지거나 빨라질 때에 실제 시간을 랜덤한 딜레이에 맞춰 적용하는 것은 불가능
            // 현재 음악이 얼마만큼 재생되었는지 나타내는 필드가 _audioSource.timeSamples
            float sampledTime = (_audioSource.timeSamples /
                                 (_audioSource.clip.frequency * interval.GetIntervalLength(_bpm)));
            interval.CheckForNewInterval(sampledTime);
        }
    }
}

[System.Serializable]
public class Intervals
{
    // 기본 BPM에서 박자 쪼개기 (0.5, 0.75 등)
    [SerializeField] private float _steps;
    [SerializeField] private UnityEvent _trigger;
    private int _lastInterval;

    public float GetIntervalLength(float bpm)
    {
        // 각각의 지정된 스텝으로 박자의 빠르기 조절(1이면 정박, 1보다 크면 빠르게, 1보다 작으면 느리게)
        return 60f / (bpm * _steps);
    }

    public void CheckForNewInterval(float interval)
    {
        //if(interval % 1 == 0) 프레임이 0.02 0.014 등으로 되기 때문에 정수가 되는 보장이 없음
        // 전의 인터벌 시간을 지나쳤다면 유니티 이벤트 함수를 실행하고 지금의 인터벌 시간이 전의 인터벌 시간이 된다
        if (Mathf.FloorToInt(interval) != _lastInterval)
        {
            _lastInterval = Mathf.FloorToInt(interval);
            _trigger.Invoke();
        }
    }
    
}
