using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BlinkText : MonoBehaviour
{
    TextMeshProUGUI tmp;
    Color color;

    public float blinkSpeed;

    // Start is called before the first frame update
    void Start()
    {
        tmp = GetComponent<TextMeshProUGUI>();
        color = tmp.color;
    }

    // Update is called once per frame
    void Update()
    {
        float alpha = Mathf.PingPong(Time.time * blinkSpeed, 1);
        color.a = alpha;
        tmp.color = color;
    }
}
