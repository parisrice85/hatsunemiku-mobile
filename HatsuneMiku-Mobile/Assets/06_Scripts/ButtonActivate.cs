using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.Feedbacks;

public class ButtonActivate : MonoBehaviour
{
    [SerializeField] private GameObject parentObj;
    private Slider _slider;
    private Button _button;
    private ColorBlock _buttonColors;
    private bool _isTouched;
    
    public MMF_Player MyPlayer;
    public float coolTime;
    public int attackNum;

    public float SliderValue
    {
        get => _slider.value;
        set => _slider.value = value;
    }

    private static readonly string noteHit = "Cube";
    
    void Awake()
    {
        _button = gameObject.AddComponent<Button>();
        _slider = GetComponentInParent<Slider>();
        _button.onClick.AddListener(ComboUp);
        ButtonColorSet();
        
        //SliderValue = 0;
        _button.interactable = true;
        
        //StartCoroutine(AttackCor());
    }

    private void OnEnable()
    {
        _isTouched = false;
        StartCoroutine(AutoDisable());
    }


    
    void ButtonColorSet()
    {
        _buttonColors = _button.colors;
        Color color = _buttonColors.disabledColor;
        color.r = 0.5f;
        color.g = 0.5f;
        color.b = 0.5f;
        _buttonColors.disabledColor = color;
        _button.colors = _buttonColors;
    }

    private void Update()
    {
        //SliderValue += Time.deltaTime * GameManager.Instance.beatManager._bpm * 0.01f;
        // 타이밍 점수를 추가한다면 노트가 활성화될 때 시간을 0에서 델타타임을 더해서
        // 일정 이하의 시간이라면 점수 ++ perfect, very good, good, slow 등의 판정
    }

    /*IEnumerator AttackCor()
    {
        yield return new WaitUntil(() => _slider.value >= 1);
        _button.interactable = true;
        // 버튼 깜빡임 애니메이션 실행
    }*/

    public void ComboUp()
    {
        if(_isTouched) return;
        _isTouched = true;
        SoundManager.instance.PlaySoundEffect(noteHit);
        // 히트 bool값을 true
        //_button.interactable = false;
        //SliderValue = 0;
        // 콤보 업
        GameManager.Instance.AddCombo(1);
        GameManager.Instance.FeverUp();
        parentObj.SetActive(false);
        //StartCoroutine(AttackCor());
        // 피드백 활용

    }
    
    // 어택이 아니라 콤보로 전환
    /*private void AttackButton()
    {
        // 버튼에 따른 공격 실행
        _slider.value = 0;
        _button.interactable = false;
        if(MyPlayer != null)
            Feedback();
        GameManager.Instance.player.Animations(attackNum);
        StartCoroutine(AttackCor());
    }*/

    private void Feedback()
    {
        if(MyPlayer == null) return;
        MyPlayer.Direction = MMFeedbacks.Directions.BottomToTop;
        MyPlayer.PlayFeedbacks();
    }
    
    IEnumerator AutoDisable()
    {
        yield return new WaitForSeconds(GameManager.Instance.beatManager._bpm * 0.01f);
        parentObj.SetActive(false);
    }
    
    private void OnDisable()
    {
        if (!_isTouched)
        {
            Debug.Log("미스!");
        }
    }
}