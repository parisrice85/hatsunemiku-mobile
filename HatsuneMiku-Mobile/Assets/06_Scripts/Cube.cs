using UnityEngine;

public class Cube : MonoBehaviour
{
    public float cubeSpeed;

    public bool onHit;

    private readonly string CubeHit = "Cube";

    //public GameObject target;
    //private readonly string TargetTag = "MainCamera";

    // Start is called before the first frame update
    /*void Start()
    {
        //target = GameObject.FindGameObjectWithTag(TargetTag);
    }
    */

    private void OnEnable()
    {
        onHit = false;
    }

    private void Start()
    {
        // 큐브 스피드 기본값
        cubeSpeed = 1;
        
        // 옵션 UI에서 설정된 PlayerPrefs 값을 참조하여 큐브의 스피드 설정
        switch (PlayerPrefs.GetInt("CubeSpeed"))
        {
            case 0:
                cubeSpeed *= 1;
                break;
            case 1:
                cubeSpeed *= 2;
                break;
            case 2:
                cubeSpeed *= 3;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //transform.forward = (target.transform.position - transform.position).normalized;
        transform.position += transform.forward * (Time.deltaTime * cubeSpeed);
    }

    public void ComboUp(int newCombo)
    {
        // 세이버에 닿았을 때 호출
        if (!onHit)
        {
            SoundManager.instance.PlaySoundEffect(CubeHit);
            // 히트 bool값을 true
            onHit = true;
            // 콤보 업
            GameManager.Instance.AddCombo(newCombo);
            GameManager.Instance.FeverUp();
        }
    }
    
    public void CubeOff()
    {
        // 세이버에 맞지 않고 일정 구간을 넘어갔다면
        if (!onHit)
        {
            GameManager.Instance.ComboReset();
            GameManager.Instance.FeverDown();
        }
        gameObject.SetActive(false);
    }
}
