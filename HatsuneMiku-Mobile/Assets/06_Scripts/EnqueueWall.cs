using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnqueueWall : MonoBehaviour
{
    private readonly string cubeRed = "CubeRed";
    private readonly string cubeBlue = "cubeBlue";

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(cubeRed))
        {
            ObjectPool.instance.cubeRed.Enqueue(other.gameObject);
            other.gameObject.GetComponent<Cube>().CubeOff();
        }
        else if (other.gameObject.CompareTag(cubeBlue))
        {
            ObjectPool.instance.cubeBlue.Enqueue(other.gameObject);
            other.gameObject.GetComponent<Cube>().CubeOff();
        }
    }
}
