using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public bool isGameover = false;
    public GameObject gameoveUI;
    public GameObject result;
    public TextMeshProUGUI comboText;
    public TextMeshProUGUI scoreText;
    public GameObject spawenr;
    public Transform cameraOffset;


    public TextMeshProUGUI maxComboText;
    public TextMeshProUGUI resultScoreText;
    public TextMeshProUGUI feverText;
    public TextMeshProUGUI totalScoreText;



    public int _combo;
    public int _score;
    private int maxCombo;

    public GameObject[] feverImages;

    private float fever;
    private float feverDiffi;
    
    StageManager stage;
    StageUI stageUI;
    Vector3 origin;
    Vector3 fever0;
    Vector3 fever1;
    Vector3 fever2;
    Vector3 fever3;

    public BeatManager beatManager;
    

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Debug.LogWarning("씬에 두 개 이상의 게임 매니저가 존재합니다!");
            Destroy(gameObject);
        }
        Time.timeScale = 1;
    }

    private void Start()
    {
        isGameover = false;

        switch (PlayerPrefs.GetInt("Difficulty"))
        {
            case 0:
                fever = 0.4f;
                feverDiffi = 0.05f;
                feverImages[1].SetActive(true);
                ImgsFillDynamic.Instance.SetValue(fever, true);
                break;
            case 1:
                feverDiffi = 0.1f;
                fever = 0.3f;
                feverImages[1].SetActive(true);
                ImgsFillDynamic.Instance.SetValue(fever, true);
                break;
            case 2:
                feverDiffi = 0.15f;
                fever = 0.2f;
                feverImages[0].SetActive(true);
                ImgsFillDynamic.Instance.SetValue(fever, true);
                break;
        }

        stageUI = FindObjectOfType <StageUI>();
        stage = FindAnyObjectByType<StageManager>();

        origin = cameraOffset.position;
        fever0 = new Vector3(cameraOffset.position.x,cameraOffset.position.y,cameraOffset.position.z + 2);
        fever1 = new Vector3(cameraOffset.position.x, cameraOffset.position.y, cameraOffset.position.z + 3);
        fever2 = new Vector3(cameraOffset.position.x, cameraOffset.position.y, cameraOffset.position.z + 4);
        fever3 = new Vector3(cameraOffset.position.x, cameraOffset.position.y, cameraOffset.position.z + 5);
    }

    private void Update()
    {
        FeverImageChange();
    }

    public void ResultOn()
    {
        spawenr.SetActive(false);
        result.SetActive(true);
        maxComboText.text = string.Format("{0:#,###}", maxCombo);
        resultScoreText.text = string.Format("{0:#,###}", _score);
        feverText.text = (fever * 100) + "%";

        int totalScore = (int)((maxCombo * 1000) + (fever * _score) + _score);

        totalScoreText.text = string.Format("{0:#,###}", totalScore);
        Time.timeScale = 0;
    }

    public void AddCombo(int newCombo)
    {
        _combo += newCombo;
        comboText.text = string.Format("{0:#,###}", _combo);
        int bonusScore = (_combo / 5) * 1000;

        if (fever >= 1)
            bonusScore *= 2;
        _score += 100 + bonusScore;
        scoreText.text = string.Format("{0:#,###}", _score);

        if(_combo > maxCombo)
        {
            maxCombo = _combo;
        }
    }

    public void ComboReset()
    {
        _combo = 0;
        comboText.text = _combo.ToString();
    }

    public void FeverUp(float _fever = 0.01f)
    {
        float bonusFever = (_combo * 0.002f);
        fever += _fever + bonusFever;

        if (fever >= 1)
            fever = 1;
        ImgsFillDynamic.Instance.SetValue(fever, false, 2);
        if(fever >= 0.8f)
        {
            stage.ActivateProps();
        }
        FeverImageChange();
    }

    public void FeverDown()
    {
        fever -= feverDiffi;
        FeverImageChange();

        if (fever <= 0.15f)
        {
            stage.DeactivateProps();
 
        }


        if (fever <= 0)
        {
            fever = 0;
            isGameover = true;
            gameoveUI.SetActive(true);
            Cube[] cubes = FindObjectsOfType<Cube>();
            for (int i = 0; i < cubes.Length; i++)
            {
                cubes[i].gameObject.SetActive(false);
            }
        }

        ImgsFillDynamic.Instance.SetValue(fever, false, 2);

    }

    private void FeverImageChange()
    {
        if (fever >= 1)
            cameraOffset.position = fever3;

        if (fever >= 0.8f)
        {
            cameraOffset.position = fever2;

            feverImages[3].SetActive(true);
            feverImages[2].SetActive(false);
            feverImages[1].SetActive(false);
            feverImages[0].SetActive(false);
        }
        else if (fever >= 0.6f)
        {
            cameraOffset.position = fever1;

            feverImages[3].SetActive(false);
            feverImages[2].SetActive(true);
            feverImages[1].SetActive(false);
            feverImages[0].SetActive(false);
        }
        else if (fever >= 0.4f)
        {
            cameraOffset.position = fever0;

            feverImages[3].SetActive(false);
            feverImages[2].SetActive(false);
            feverImages[1].SetActive(true);
            feverImages[0].SetActive(false);
        }
        else
        {
            cameraOffset.position = origin;

            feverImages[3].SetActive(false);
            feverImages[2].SetActive(false);
            feverImages[1].SetActive(false);
            feverImages[0].SetActive(true);
        }
    }

    // 피버 관리

    // 곡 선택 관리

    // 메뉴 관리

    // 소품 관리(네코미미, 네기 etc..)
}
