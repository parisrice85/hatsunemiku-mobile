using uLipSync;
using UnityEngine;

public class NekomimiHeadPhone : MonoBehaviour
{
    public GameObject nekomimi;
    public uLipSyncBlendShape uLipSyncBlendShape;
    public uLipSyncAudioSource uLipSyncAudioSource;

    public GameObject[] negi;


    // Start is called before the first frame update
    void Start()
    {
        // 옵션 UI에서 설정된 PlayerPrefs 값을 참조하여 스테이지 소품 결정
        switch (PlayerPrefs.GetInt("SelectSong"))
        {
            case 0:
                nekomimi.SetActive(false);
                uLipSyncBlendShape.enabled = true;
                uLipSyncAudioSource.enabled= true;
                break;
            case 1:
                nekomimi.SetActive(true);
                uLipSyncBlendShape.enabled = false;
                uLipSyncAudioSource.enabled = false;
                break;
            case 2:
                nekomimi.SetActive(false);
                uLipSyncBlendShape.enabled = false;
                uLipSyncAudioSource.enabled = false;
                break;
        }

        foreach (GameObject negi in negi) { negi.SetActive(false); }
    }

    public void CallResult()
    {
        GameManager.Instance.ResultOn();
    }

    public void CubeStop()
    {
        GameManager.Instance.isGameover = true;
    }

    public void CubeStart()
    {
        GameManager.Instance.isGameover = false;
    }

    public void RightNegiAppear()
    {
        negi[0].SetActive(true);
    }

    public void LeftNegiAppear()
    {
        negi[1].SetActive(true);
    }

    public void NegiDisappear()
    {
        foreach(GameObject negi in negi) { negi.SetActive(false); }
    }
}
