using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectInfo
{
    public GameObject goPrefab;
    public int count;
    public Transform tfPoolParent;
}

public class ObjectPool : MonoBehaviour
{
    public static ObjectPool instance;

    [SerializeField] ObjectInfo[] objectInfo = null;

    public Queue<GameObject> cubeRed = new Queue<GameObject>();
    public Queue<GameObject> cubeBlue = new Queue<GameObject>();


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        cubeRed = InsertQueue(objectInfo[0]);
        cubeBlue = InsertQueue(objectInfo[1]);
    }

    Queue<GameObject> InsertQueue(ObjectInfo p_objectInfo)
    {
        Queue<GameObject> t_queue = new Queue<GameObject>();

        for (int i = 0; i < p_objectInfo.count; i++)
        {
            GameObject t_clone = Instantiate(p_objectInfo.goPrefab);
            t_clone.transform.Rotate(transform.up, 180);
            t_clone.SetActive(false);
            if (p_objectInfo.tfPoolParent != null)
            {
                t_clone.transform.SetParent(p_objectInfo.tfPoolParent);
            }
            else
            {
                t_clone.transform.SetParent(this.transform);

            }

            t_queue.Enqueue(t_clone);
        }

        return t_queue;
    }
}
