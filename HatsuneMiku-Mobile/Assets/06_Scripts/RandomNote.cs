using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RandomNote : MonoBehaviour
{
    [SerializeField] private GameObject[] notes;

    private int _noteNum;
    private int _preNum;

    private void Start()
    {
        // 처음은 어떤 노트라도 출현 가능
        // 두 번째 노트부터 한 번의 중복만 피함
        _preNum = -1;
    }

    public void RandomActiveNote()
    {
        while (true)
        {
            _noteNum = Random.Range(0, notes.Length);
            if (_noteNum != _preNum)
            {
                notes[_noteNum].SetActive(true);
                _preNum = _noteNum;
            }
            else
            {
                continue;
            }
            break;
        }
    }
}
