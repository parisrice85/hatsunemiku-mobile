using UnityEngine;


public class Saber : MonoBehaviour
{
    // 큐브 레이어
    public LayerMask layer;
    // 1프레임 전의 세이버 위치
    private Vector3 previousPos;

    // 메모리 최적화를 위한 string 변수
    private readonly string cubeRed = "CubeRed";
    private readonly string cubeBlue = "CubeBlue";
    private readonly int OnHit = Animator.StringToHash("OnHit");
    

    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        // 세이버에서 레이를 발사하여 큐브의 레이어를 검사
        if (Physics.Raycast(transform.position, transform.forward, out hit, 1, layer))
        {
            // 현재 세이버를 내려치는 방향에서 hit.transform.up(즉, 윗방향)의 130도 각도를 계산
            if (Vector3.Angle(transform.position - previousPos, hit.transform.up) > 130)
            {
                // 큐브의 태그가 레드라면
                if (hit.transform.gameObject.CompareTag(cubeRed))
                {
                    // 오브젝트 풀 담당에서 큐브를 꺼냄
                    ObjectPool.instance.cubeRed.Enqueue(hit.transform.gameObject);
                    // 콤보 업
                    hit.transform.GetComponent<Cube>().ComboUp(1);
                    // 큐브 피격 애니메이션
                    hit.transform.GetComponent<Animator>().SetTrigger(OnHit);
                    // 컨트롤러 진동

                }
                // 큐브의 태그가 블루라면
                else if (hit.transform.gameObject.CompareTag(cubeBlue))
                {
                    // 이하 레드와 같음
                    ObjectPool.instance.cubeBlue.Enqueue(hit.transform.gameObject);
                    hit.transform.GetComponent<Cube>().ComboUp(1);
                    hit.transform.GetComponent<Animator>().SetTrigger(OnHit);

                }
            }
        }
        // 세이버의 방향을 위한 전 프레임 벡터값 저장
        previousPos = transform.position;
    }

    // return : -180 ~ 180 degree (for unity)
    /*public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }*/
}
