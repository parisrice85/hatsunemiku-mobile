using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableTicket : ScriptableObject
{
    public int songNum;
    public Sprite songSprite;
    public string songName;
    // 최고 기록 추가
}
