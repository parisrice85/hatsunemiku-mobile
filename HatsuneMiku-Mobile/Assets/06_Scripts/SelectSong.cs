using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SelectSong : MonoBehaviour
{
    // 티켓은 여러장(곡 수)
    [SerializeField] private ScriptableTicket[] tickets;
    // 해당 UI의 곡 그림
    private Sprite _songPic;
    // 해당 UI의 곡 이름
    private TextMeshProUGUI _songName;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
