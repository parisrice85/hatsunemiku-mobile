using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] cubes;
    public Transform[] points;
    private float beat;
    private float timer;

    public float Beat
    {
        get { return beat; }
        set { beat = value; }
    }

    private void Start()
    {
        switch (PlayerPrefs.GetInt("Difficulty"))
        {
            case 0:
                beat *= 2f;
                break;
            case 1:
                beat *= 1.5f;
                break;
            case 2:
                beat *= 1f;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.isGameover) return;

        if (timer > beat)
        {
            /*// 큐브 생성을 (빨강, 파랑)색 랜덤과 4군데의 랜덤 포지션(points)으로 함
            var cube = Instantiate(cubes[Random.Range(0, 2)], points[Random.Range(0, 4)]);
            // 스포너에 있는 각 points의 자리에서 출발하도록
            cube.transform.localPosition = Vector3.zero;
            // 큐브의 방향은 플레이어를 향하고 방향은 상하좌우 랜덤으로 출현
            cube.transform.Rotate(transform.forward, 90 * Random.Range(0,4));
            // 곡의 비트값으로 큐브 생성 주기 조절
            timer -= beat;*/

            int rand = Random.Range(0, 2);

            if(rand == 1)
            {
                if(ObjectPool.instance.cubeRed.Count <= 0) return;
                
                int rand1 = Random.Range(0, 4);

                GameObject tCube = ObjectPool.instance.cubeRed.Dequeue();
                if (tCube.activeSelf)
                {
                    timer = beat;
                    return;
                }
                tCube.transform.SetParent(points[rand1].transform);
                tCube.transform.localPosition = points[rand1].position;
                tCube.transform.localPosition = Vector3.zero;
                tCube.transform.Rotate(transform.forward, 90 * Random.Range(0, 4));
                //t_cube.transform.Rotate(transform.up, 180);
                tCube.SetActive(true);
            }
            else
            {
                if(ObjectPool.instance.cubeBlue.Count <= 0) return;
                
                int rand1 = Random.Range(0, 4);
                
                GameObject tCube = ObjectPool.instance.cubeBlue.Dequeue();
                if (tCube.activeSelf)
                {
                    timer = beat;
                    return;
                }
                tCube.transform.SetParent(points[rand1].transform);
                tCube.transform.localPosition = points[rand1].position;
                tCube.transform.localPosition = Vector3.zero;
                tCube.transform.Rotate(transform.forward, 90 * Random.Range(0, 4));
                //t_cube.transform.Rotate(transform.up, 180);
                tCube.SetActive(true);
            }
            

            timer -= beat;

        }

        timer += Time.deltaTime;
    }
}
