using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageManager : MonoBehaviour
{
    // Control options.
    public bool ignoreFastForward = true;

    // BGM 담당 프리팹
    public GameObject musicPlayerPrefab;
    // 무대 프리팹
    public GameObject[] prefabsNeedsActivation;
    
    public GameObject[] prefabsOnTimeline;
    public GameObject[] miscPrefabs;
    
    // 점수에 따라 카메라가 미쿠에게 접근하는 정도
    public Transform[] cameraPoints;

    // 뮤직플레이어
    GameObject musicPlayer;
    // 조명 스테이지
    GameObject[] objectsNeedsActivation;
    // 실사간으로 무대를 진행하는 프리팹
    GameObject[] objectsOnTimeline;
    GameObject mikuChan;

    // 스테이지 오디오
    private List<AudioSource> audios;

    // 곡 선택
    [Tooltip("플레이 곡")]
    public AudioClip[] clips;
    [Tooltip("댄스 모션")]
    public AnimatorOverrideController[] _motions;
    // PlayerPrefs 에서 받은 값으로 스테이지 곡 설정
    private const int CCC = 0, NekoMimi = 1, Ievan = 2;
    private AudioClip selectSong;
    private Animator _mikuAnimator;

    // 비트 조절용 스포너
    public Spawner _spawner;

    void Awake()
    {
        // 무대 장치 생성
        if (musicPlayer != null)
        {
            musicPlayer = (GameObject)Instantiate(musicPlayerPrefab);
        }
        objectsNeedsActivation = new GameObject[prefabsNeedsActivation.Length];
        for (var i = 0; i < prefabsNeedsActivation.Length; i++)
            objectsNeedsActivation[i] = (GameObject)Instantiate(prefabsNeedsActivation[i]);
        
        objectsOnTimeline = new GameObject[prefabsOnTimeline.Length];
        for (var i = 0; i < prefabsOnTimeline.Length; i++)
            objectsOnTimeline[i] = (GameObject)Instantiate(prefabsOnTimeline[i]);
        

        foreach (var p in miscPrefabs) Instantiate(p);
        
        // 곡에 맞는 애니메이션 설정
        mikuChan = objectsOnTimeline[0];
        _mikuAnimator = mikuChan.GetComponent<Animator>();
        SelectSong();
    }
    
    // 애니메이션 설정용 오버라이드컨트롤러
    void SetMotions(AnimatorOverrideController overrideController)
    {
        _mikuAnimator.runtimeAnimatorController = overrideController;
    }

    // 메인메뉴에서 받은 곡으로 설정
    void SelectSong()
    {
        // 옵션 UI에서 설정된 PlayerPrefs 값을 참조하여 해당 곡과 비트 설정
        switch (PlayerPrefs.GetInt("SelectSong"))
        {
            case CCC:
                selectSong = clips[CCC];
                SetMotions(_motions[CCC]);
                _spawner.Beat = 0.455f;
                break;

            case NekoMimi:
                selectSong = clips[NekoMimi];
                SetMotions(_motions[NekoMimi]);
                _spawner.Beat = 0.75f;
                break;

            case Ievan:
                selectSong = clips[Ievan];
                SetMotions(_motions[Ievan]);
                _spawner.Beat = 0.505f;
                break;
        }
    }

    public void ReStartMusic()
    {
        foreach (var source in audios)
        {
            source.Play();
        }
    }

    public void PauseMusic()
    {
        foreach (var source in audios)
        {
            source.Pause();
        }
    }


    //===애니메이션 이벤트 메서드==========================

    public void StartMusic()
    {
        audios = new List<AudioSource>();

        foreach (var source in musicPlayer.GetComponentsInChildren<AudioSource>())
        {
            audios.Add(source);
        }
        audios.Add(mikuChan.GetComponent<AudioSource>());
        
        /*foreach (var source in musicPlayer.GetComponentsInChildren<AudioSource>())
            source.Play();*/
        
        foreach (var source in audios)
        {
            source.clip = selectSong;
            source.Play();
        }
    }

    public void ActivateProps()
    {
        foreach (var o in objectsNeedsActivation) o.BroadcastMessage("ActivateProps");
    }

    public void DeactivateProps()
    {
        foreach (var o in objectsNeedsActivation) o.BroadcastMessage("DeactivateProps");
    }

    public void FastForward(float second)
    {
        if (!ignoreFastForward)
        {
            FastForwardAnimator(GetComponent<Animator>(), second, 0);
            foreach (var go in objectsOnTimeline)
                foreach (var animator in go.GetComponentsInChildren<Animator>())
                    FastForwardAnimator(animator, second, 0.5f);
        }
    }

    void FastForwardAnimator(Animator animator, float second, float crossfade)
    {
        for (var layer = 0; layer < animator.layerCount; layer++)
        {
            var info = animator.GetCurrentAnimatorStateInfo(layer);
            if (crossfade > 0.0f)
                animator.CrossFade(info.fullPathHash, crossfade / info.length, layer, info.normalizedTime + second / info.length);
            else
                animator.Play(info.fullPathHash, layer, info.normalizedTime + second / info.length);
        }
    }

    public void EndPerformance()
    {
        //Application.LoadLevel(0);
        SceneManager.LoadScene(0);
    }

    public void CubeSpawnerOn()
    {
        
    }
    public void CubeSpawnerOff()
    {
        
    }
}
