using TMPro;
using UnityEngine;
//using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class StageUI : MonoBehaviour
{
    public GameObject background;
    public GameObject pause;
    public GameObject option;
    public GameObject gameover;

    public GameObject[] cubeSpeedBtns;
    public GameObject[] difficultyBtns;

    //[SerializeField] InputActionReference InputActionReference;
    [SerializeField] StageManager stageManager;

    private readonly string button = "Button";
    private readonly string cancel = "Cancel";
    private readonly string WaittingRoom = "WaittingRoom";
    private readonly string CubeSpeed = "CubeSpeed";
    private readonly string Difficulty = "Difficulty";


    // Start is called before the first frame update
    void OnEnable()
    {
        CubeAlpha(PlayerPrefs.GetInt(CubeSpeed));
        DiffiAlpha(PlayerPrefs.GetInt(Difficulty));
    }

    private void Start()
    {
        //InputActionReference.action.performed += OnClick;
    }

    //======= 버튼 함수===================

    public void CubeSpeedBtn(int speed)
    {
        SoundManager.instance.PlaySoundEffect(button);

        PlayerPrefs.SetInt(CubeSpeed, speed);
        CubeAlpha(speed);
    }
    public void DifficultyBtn(int difficulty)
    {
        SoundManager.instance.PlaySoundEffect(button);

        PlayerPrefs.SetInt(Difficulty, difficulty);
        DiffiAlpha(difficulty);
    }

    // 메뉴 바깥을 클릭했을 때
    public void AllOff()
    {
        SoundManager.instance.PlaySoundEffect(button);

        option.SetActive(false);
        pause.SetActive(false);
        background.SetActive(false);
        // 시간을 원래대로
        stageManager.ReStartMusic();
        Time.timeScale = 1;
        
    }

    public void OptionBtn()
    {
        SoundManager.instance.PlaySoundEffect(button);

        pause.SetActive(false);
        option.SetActive(true);
    }

    public void GameoverOptionBtn()
    {
        SoundManager.instance.PlaySoundEffect(button);

        gameover.SetActive(false);
        option.SetActive(true);
    }

    public void OptionCancelBtn()
    {
        SoundManager.instance.PlaySoundEffect(cancel);

        if (!GameManager.Instance.isGameover)
        {
            option.SetActive(false);
            pause.SetActive(true);
        }
        else
        {
            option.SetActive(false);
            gameover.SetActive(true);
        }
        
    }

    public void GoTitleBtn()
    {
        SoundManager.instance.PlaySoundEffect(button);

        SceneManager.LoadScene(WaittingRoom);
    }

    public void ReStart()
    {
        SoundManager.instance.PlaySoundEffect(button);

        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    //====================================

    void CubeAlpha(int index)
    {
        for (int i = 0; i < cubeSpeedBtns.Length; i++)
        {
            if (i == index)
            {
                Color color = cubeSpeedBtns[i].GetComponent<Image>().color;
                color.a = 1;
                cubeSpeedBtns[i].GetComponent<Image>().color = color;
                Color color2 = cubeSpeedBtns[i].GetComponentInChildren<TextMeshProUGUI>().color;
                color2.a = 1;
                cubeSpeedBtns[i].GetComponentInChildren<TextMeshProUGUI>().color = color2;
            }
            else
            {
                Color color = cubeSpeedBtns[i].GetComponent<Image>().color;
                color.a = 0.1f;
                cubeSpeedBtns[i].GetComponent<Image>().color = color;
                Color color2 = cubeSpeedBtns[i].GetComponentInChildren<TextMeshProUGUI>().color;
                color2.a = 0.1f;
                cubeSpeedBtns[i].GetComponentInChildren<TextMeshProUGUI>().color = color2;
            }
        }
    }

    void DiffiAlpha(int index)
    {
        for (int i = 0; i < difficultyBtns.Length; i++)
        {
            if (i == index)
            {
                Color color = difficultyBtns[i].GetComponent<Image>().color;
                color.a = 1;
                difficultyBtns[i].GetComponent<Image>().color = color;
                Color color2 = difficultyBtns[i].GetComponentInChildren<TextMeshProUGUI>().color;
                color2.a = 1;
                difficultyBtns[i].GetComponentInChildren<TextMeshProUGUI>().color = color2;
            }
            else
            {
                Color color = difficultyBtns[i].GetComponent<Image>().color;
                color.a = 0.1f;
                difficultyBtns[i].GetComponent<Image>().color = color;
                Color color2 = difficultyBtns[i].GetComponentInChildren<TextMeshProUGUI>().color;
                color2.a = 0.1f;
                difficultyBtns[i].GetComponentInChildren<TextMeshProUGUI>().color = color2;
            }
        }
    }
    
    // 버튼으로 대체 예정
    //================================================
    // 오큘러스 컨트롤러를 이용해서 게임 중 메뉴 호출
    /*private void OnClick(InputAction.CallbackContext obj)
    {
        if (GameManager.Instance.isGameover) return; 
        if (background == null || pause == null) return;
        if (background.activeSelf || pause.activeSelf || gameover.activeSelf) return;

        SoundManager.instance.PlaySoundEffect(button);

        stageManager.PauseMusic();
        Time.timeScale = 0;
        background.SetActive(true);
        pause.SetActive(true);
    }*/
}
