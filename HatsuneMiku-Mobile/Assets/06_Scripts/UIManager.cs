using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("UI 오브젝트")]
    public GameObject title;
    public GameObject startBtn;
    public GameObject songOption;
    
    [Header("곡의 옵션 버튼")]
    public GameObject[] cubeSpeedBtns;
    public GameObject[] difficultyBtns;
    
    [Header("티켓 구성")]
    [SerializeField] private GameObject[] ticket;
    [SerializeField] private ScriptableTicket[] _scriptableTicketstickets;
    [SerializeField] private Image songPicture;
    [SerializeField] private TextMeshProUGUI songName;
    private int _songNum;
    
    
    private readonly int Appear = Animator.StringToHash("Appear");
    private readonly int Disappear = Animator.StringToHash("DisAppear");
    private readonly string button = "Button";
    private readonly string cancel = "Cancel";
    private readonly string Onstage = "Onstage";
    private readonly string CubeSpeed = "CubeSpeed";
    private readonly string Difficulty = "Difficulty";
    private readonly string SelectSong = "SelectSong";

    private void Awake()
    {
        Time.timeScale = 1;
        PlayerPrefs.SetInt(CubeSpeed, 0);
        PlayerPrefs.SetInt(Difficulty, 0);
    }

    void TicketOn()
    {
        for (int i = 0; i < ticket.Length; i++)
        {
            ticket[i].GetComponent<Animator>().SetTrigger(Appear);
        }
    }

    void TicketOff()
    {
        for (int i = 0; i < ticket.Length; i++)
        {
            ticket[i].GetComponent<Animator>().SetTrigger(Disappear);
        }
    }

    //===========================================================
    // 버튼 함수

    public void StageStart()
    {
        SoundManager.instance.PlaySoundEffect(button);
        SceneManager.LoadScene(Onstage);
    }

    public void StartBtn()
    {
        StartCoroutine(StartClick());
    }

    IEnumerator StartClick()
    {
        float a = 1;
        Color color = title.GetComponentInChildren<Image>().color;
        Color color2 = title.GetComponentInChildren<TextMeshProUGUI>().color;

        // 효과음
        SoundManager.instance.PlaySoundEffect(button);

        while (a > 0)
        {
            a -= 0.1f;
            color.a = a;
            color2.a = a;
            yield return new WaitForSeconds(0.05f);
        }

        title.gameObject.SetActive(false);
        startBtn.SetActive(false);
        
        // PlayerPrefs값으로 티켓 유지
        ChangeTicket();
        
        for (int i = 0; i < ticket.Length; i++)
        {
            ticket[i].SetActive(true);
        }
    }

    public void CancelBtn()
    {
        SoundManager.instance.PlaySoundEffect(cancel);

        songOption.SetActive(false);
        TicketOn();
    }
    
    // 체인지 송 버튼으로 바꿔서 매개 변수 없이 SongNum++
    public void SongChangeBtn(int changeValue)
    {
        SoundManager.instance.PlaySoundEffect(button);
        _songNum += changeValue;
        PlayerPrefs.SetInt(SelectSong, _songNum);
        
        ChangeTicket();
    }

    void ChangeTicket()
    {
        for (int i = 0; i < _scriptableTicketstickets.Length; i++)
        {
            if(_songNum != _scriptableTicketstickets[i].songNum) continue;
            songPicture.sprite = _scriptableTicketstickets[i].songSprite;
            songName.text = _scriptableTicketstickets[i].songName;
            break;
        }
    }
    
    public void SongClickBtn()
    {
        // 티켓 없어지는 애니메이션
        TicketOff();
        
        // 옵션 UI 활성화
        songOption.SetActive(true);
    }
    
    public void CubeSpeedBtn(int speed)
    {
        SoundManager.instance.PlaySoundEffect(button);

        PlayerPrefs.SetInt(CubeSpeed, speed);
        CubeAlpha(speed);
    }
    public void DifficultyBtn(int difficulty)
    {
        SoundManager.instance.PlaySoundEffect(button);

        PlayerPrefs.SetInt(Difficulty, difficulty);
        DiffiAlpha(difficulty);
    }

    void CubeAlpha(int index)
    {
        for (int i = 0; i < cubeSpeedBtns.Length; i++)
        {
            if(i == index)
            {
                Color color = cubeSpeedBtns[i].GetComponent<Image>().color;
                color.a = 1;
                cubeSpeedBtns[i].GetComponent<Image>().color = color;
                Color color2 = cubeSpeedBtns[i].GetComponentInChildren<TextMeshProUGUI>().color;
                color2.a = 1;
                cubeSpeedBtns[i].GetComponentInChildren<TextMeshProUGUI>().color = color;
            }
            else
            {
                Color color = cubeSpeedBtns[i].GetComponent<Image>().color;
                color.a = 0.1f;
                cubeSpeedBtns[i].GetComponent<Image>().color = color;
                Color color2 = cubeSpeedBtns[i].GetComponentInChildren<TextMeshProUGUI>().color;
                color2.a = 0.1f;
                cubeSpeedBtns[i].GetComponentInChildren<TextMeshProUGUI>().color = color;
            }
        }      
    }

    void DiffiAlpha(int index)
    {
        for (int i = 0; i < difficultyBtns.Length; i++)
        {
            if (i == index)
            {
                Color color = difficultyBtns[i].GetComponent<Image>().color;
                color.a = 1;
                difficultyBtns[i].GetComponent<Image>().color = color;
                Color color2 = difficultyBtns[i].GetComponentInChildren<TextMeshProUGUI>().color;
                color2.a = 1;
                difficultyBtns[i].GetComponentInChildren<TextMeshProUGUI>().color = color;
            }
            else
            {
                Color color = difficultyBtns[i].GetComponent<Image>().color;
                color.a = 0.1f;
                difficultyBtns[i].GetComponent<Image>().color = color;
                Color color2 = difficultyBtns[i].GetComponentInChildren<TextMeshProUGUI>().color;
                color2.a = 0.1f;
                difficultyBtns[i].GetComponentInChildren<TextMeshProUGUI>().color = color;
            }
        }
    }
}
